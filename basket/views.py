# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from myrm.basket_operations import BasketManager
from myrm.move_errors import Error
from django.shortcuts import render_to_response, render, redirect
from django.http import HttpResponseRedirect
from django .views import generic
from .models import Basket, Task
from .forms import TaskForm, BasketForm
from .task_states import TaskState
import multiprocessing


def home_page(request):
    return render('basket/home.html')


def baskets_list(request):
    baskets = {
        'baskets': Basket.object.order_by('name')
    }

    return render('basket/basket_list.html', baskets)


def basket_detail(request, pk):
    basket = Basket.objects.get(pk=int(pk))
    form = BasketForm(instance=basket)

    if request.method == "POST":
        form = BasketForm(request.POST, instance=basket)

        if form.is_valid():
            form.save()

            return redirect('/basket/')

    trash = BasketManeger(
        rmdir=basket.rmdir,
        force = basket.force,
        dry_run = basket.dry_run,
        max_size = basket.max_size,
        storage_time = basket.storage_time,
        basket_path = basket.basket_path,
    )

    context = {
        'form': form,
        'basket': trash,
        'baskets': Basket.objects.all()
    }

    return render(request, 'basket/basket_detail.html', context)


def basket_new(request):
    form = BasketForm()
    if request.method == "POST":
        form = BasketForm(request.POST)
        if form.is_valid():
            basket = form.save()
            basket.save()

            return redirect('/baskets/')

    return render(request, 'basket/basket_new.html', {'form': form})


def task_detail(request, pk):
    task = Task.object.get(pk=int(pk))
    form = TaskForm(instance=task)

    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)

        if form.is_valid:
            form.save()
            return redirect('/task/')

    return render(request, 'basket/task_detail.html', {'form': form})


def task_list(request):
    tasks = {
        'tasks':Task.object.order_by('name'),
    }
    return render('basket/task_detail.html', tasks)


def task_new(request):
    form = TaskForm()

    if request.method == 'POST':
        form = TaskForm(request.POST)

        if form.is_valid():
            task = form.save()
            task.save()

            return HttpResponseRedirect('/task/')

    return render(request, 'basket/task_new.html', {'form': form})


def task_result(request, pk):
    task = Task.objects.get(pk=pk)

    working_task = basket_working_tasks(task.basket)

    context = {}
    if working_task:
        context = { 'task': working_task[0]}
    else:
        task.state = TaskState.WORKING
        task.save()

        context = parallel_of_task(task)

        task.state = TaskState.COMPLETED
        task.save()

    return render(request, 'basket/task_result.html', context)


def parallel_of_task(task, num_process=None):
    mgr = multiprocessing.Manager()
    result = mgr.list()
    trash = basket_from_task(task)
    paths = task.paths.split()

    if num_process is None:
        num_process = multiprocessing.cpu_count() * 2

    action, error_object = None, None

    if task.restore:
        action = 'restore'

        splited_paths = [[] for i in xrange(num_process)]
        for i in xrange(len(paths)):
            splited_paths[i % num_process].append(paths[i])

        jobs = []
        for i in xrange(num_process):
            j = multiprocessing.Process(target=restore_worker,
                                        args=(result, trash, splited_paths[i]))
            jobs.append(j)

        for j in jobs:
            j.start()

        for j in jobs:
            j.join()
    elif task.regex:
        action = 'remove'
        result = trash.remove_regex(paths[0], search_dirs=True)
    else:
        action = 'remove'

        while paths:
            paths, leafs = cut_leafs(*paths)

            splited_paths = [[] for i in xrange(num_process)]
            for i in xrange(len(leafs)):
                splited_paths[i % num_process].append(leafs[i])

            jobs = []
            for i in xrange(num_process):
                j = multiprocessing.Process(target=remove_worker,
                                            args=(result, trash, splited_paths[i]))
                jobs.append(j)

            for j in jobs:
                j.start()

            for j in jobs:
                j.join()

    context = {
        'result': result,
        'action': action,
        'error_object': error_object,
    }

    return context


def basket_from_task(task):
    """Creates and returns BasketManager object from task.
    Task flags overlap BaskeModel flags.
    """
    basket_model = task.basket

    trash = BasketManager(
        rmdir=basket_model.rmdir,
        force=basket_model.force,
        dry_run=basket_model.dry_run,
        max_size=basket_model.max_size,
        storage_time=basket_model.storage_time,
        basket_path=basket_model.basket_path,
    )

    if task.rmdir is not None:
        trash.rmdir = task.rmdir

    if task.force is not None:
        trash.force = task.force

    if task.dry_run is not None:
        trash.dry_run = task.dry_run

    return trash


def basket_working_tasks(basket_model):
    """Checks if wastebasket_model has a working tasks.
    Returns the list of running task.
    """

    tasks = Task.objects.filter(basket__exact=basket_model)

    working_tasks = []
    for task in tasks:
        if task.state == TaskState.WORKING:
            working_tasks.append(task)

    return working_tasks


def restore_worker(result, trash, paths):
    """Function for parallel restoring files."""

    result.extend(trash.restore(*paths))


def remove_worker(result, trash, paths):
    """Function for restoring files."""
    result.extend(trash.remove(*paths))
