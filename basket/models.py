# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from .task_states import TaskState
import datetime


class Basket(models.Model):
    name = models.CharField(max_length=50, default='Basket')
    rmdir = models.BooleanField()
    dry_run = models.BooleanField()
    force = models.BooleanField()

    max_size = models.IntegerField(default=10000, name='max_size')
    storage_time = models.DurationField(default=datetime.timedelta(days=14))
    basket_path = models.FilePathField(path='/home/miroslav')

    def __unicode__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=50, default='Task')
    state = models.IntegerField(default=TaskState.CREATED)

    basket = models.ForeignKey(Basket)

    rmdir = models.NullBooleanField()
    dry_run = models.NullBooleanField()
    force = models.NullBooleanField()

    restore = models.BooleanField()
    regex = models.BooleanField()

    path = models.TextField()

    def __unicode__(self):
        return self.name