# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import home_page, basket_detail, baskets_list, basket_new, task_list, task_new, task_detail


app_name = 'basket'
urlpatterns = [
    url(r'^$', views.home_page, name='home'),
    url(r'^baskets/$', views.baskets_list, name='baskets_list'),
    url(r'^baskets/(?P<pk>\d+)/$', views.basket_detail, name='basket_detail'),
    url(r'^baskets/new/$', views.basket_new, name='basket_new'),
    url(r'^task/$', views.task_list, name='task_list'),
    url(r'^task/(?P<pk>\d+)/$', views.task_detail, name='task_detail'),
    url(r'^task/new/$', views.task_new, name='task_new'),

]