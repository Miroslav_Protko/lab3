# -*- coding: utf-8 -*-

from django import forms
from .models import Basket, Task

class TaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ('name', 'basket', 'rmdir', 'force', 'dry_run', 'restore', 'regex', 'paths')


class BasketForm(forms.ModelForm):

    class Meta:
        model = Basket
        fields = ('name', 'rmdir', 'force', 'dry_run', 'max_size', 'storage_time', 'basket_path')