##Django smartrm

Modified program to delete files and folders with the addition of a web interface and parallel execution.

###List of basic operations:

* View known trashbins.
* Create, manage, display status and removal of trashbins.
* View tasks.
* Create, manage and display the status of tasks.
* Parallelizing by directories.

##Start

```
python manage.py runserver
```

##Recomendation
Install djando version 1.11
```
pip install django=1.11
```


##Instruction

The start page contains list of created trash. Your can get trash content by click on it's name.
Click on 'Basket' in the upper left corner of the screen for creating new trash.
You can create a regular expression for convenient removal of multiple files.


### Default config file

JSON:

{
    "clear": False,
    "show": False,
    "dry_run": False,
    "force": False,
    "policy": "time",
    "max_size": 10000,
    "regex": False,
    "restore": False,
    "silent": False,
    "storage_time": "14 days, 0:0:0",
    "basket_path": "/home/miroaslav/basket"
}

